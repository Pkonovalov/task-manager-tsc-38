package ru.pkonovalov.tm;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pkonovalov.tm.endpoint.AdminUserEndpoint;
import ru.pkonovalov.tm.endpoint.Session;
import ru.pkonovalov.tm.marker.SoapCategory;

public class AdminUserEndpointTest extends AbstractEndpointTest {

    @NotNull
    private static final AdminUserEndpoint ADMIN_USER_ENDPOINT = BOOTSTRAP.getAdminUserEndpoint();

    @Test(expected = ServerSOAPFaultException.class)
    @Category(SoapCategory.class)
    public void denyLockTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        ADMIN_USER_ENDPOINT.lockUserByLogin(SESSION, ADMIN_USER_NAME);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(SoapCategory.class)
    public void denyUnlockTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        ADMIN_USER_ENDPOINT.unlockUserByLogin(SESSION, ADMIN_USER_NAME);
    }

    @After
    public void finishTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
    }

    @Test
    @Category(SoapCategory.class)
    public void lockTest() {
        ADMIN_USER_ENDPOINT.lockUserByLogin(SESSION, TEST_USER_NAME);
        @Nullable Session session = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        Assert.assertNull(session);
        ADMIN_USER_ENDPOINT.unlockUserByLogin(SESSION, TEST_USER_NAME);
        session = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        Assert.assertNotNull(session);
    }

    @Before
    public void startTest() {
        SESSION = SESSION_ENDPOINT.openSession(ADMIN_USER_NAME, ADMIN_USER_PASSWORD);
    }

}
