package ru.pkonovalov.tm;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pkonovalov.tm.endpoint.Project;
import ru.pkonovalov.tm.endpoint.ProjectEndpoint;
import ru.pkonovalov.tm.endpoint.Session;
import ru.pkonovalov.tm.endpoint.Status;
import ru.pkonovalov.tm.marker.SoapCategory;

public class ProjectEndpointTest extends AbstractEndpointTest {

    @NotNull
    private static final ProjectEndpoint PROJECT_ENDPOINT = BOOTSTRAP.getProjectEndpoint();
    @NotNull
    private static final String TEST_DESCRIPTION = "Test project description";
    @NotNull
    private static final String TEST_NAME = "Test project name";
    @NotNull
    private static final String TEST_NAME_TWO = "Test project name 2";
    @NotNull
    private static Project TEST_PROJECT = PROJECT_ENDPOINT.createProject(SESSION, TEST_NAME, TEST_DESCRIPTION);
    @NotNull
    private static String TEST_PROJECT_ID = TEST_PROJECT.getId();

    @Test(expected = ServerSOAPFaultException.class)
    @Category(SoapCategory.class)
    public void clearProjectsWithInvalidSession() {
        @NotNull final Session emptySession = new Session();
        BOOTSTRAP.getProjectEndpoint().clearProject(emptySession);
    }

    @Test
    @Category(SoapCategory.class)
    public void countProject() {
        PROJECT_ENDPOINT.clearProject(SESSION);
        Assert.assertEquals(0, PROJECT_ENDPOINT.countProject(SESSION));
        PROJECT_ENDPOINT.createProject(SESSION, TEST_NAME, TEST_DESCRIPTION);
        Assert.assertEquals(1, PROJECT_ENDPOINT.countProject(SESSION));
        PROJECT_ENDPOINT.createProject(SESSION, TEST_NAME_TWO, TEST_DESCRIPTION);
        Assert.assertEquals(2, PROJECT_ENDPOINT.countProject(SESSION));
        PROJECT_ENDPOINT.clearProject(SESSION);
        Assert.assertEquals(0, PROJECT_ENDPOINT.countProject(SESSION));
    }

    @Test
    @Category(SoapCategory.class)
    public void createTest() {
        @NotNull final Project project = PROJECT_ENDPOINT.createProject(SESSION, TEST_NAME, TEST_DESCRIPTION);
        Assert.assertNotNull(project);
        Assert.assertEquals(TEST_NAME, project.getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProject() {
        PROJECT_ENDPOINT.finishProjectById(SESSION, TEST_PROJECT_ID);
        Assert.assertEquals(Status.COMPLETE, PROJECT_ENDPOINT.findProjectById(SESSION, TEST_PROJECT_ID).getStatus());
    }

    @After
    public void finishTest() {
        PROJECT_ENDPOINT.clearProject(SESSION);
    }

    @Test
    @Category(SoapCategory.class)
    public void startProject() {
        PROJECT_ENDPOINT.startProjectById(SESSION, TEST_PROJECT.getId());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_ENDPOINT.findProjectById(SESSION, TEST_PROJECT_ID).getStatus());
    }

    @Before
    public void startTest() {
        TEST_PROJECT = PROJECT_ENDPOINT.createProject(SESSION, TEST_NAME, TEST_DESCRIPTION);
        TEST_PROJECT_ID = TEST_PROJECT.getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void updateProject() {
        Assert.assertEquals(TEST_NAME, TEST_PROJECT.getName());
        PROJECT_ENDPOINT.updateProjectById(SESSION, TEST_PROJECT_ID, TEST_NAME_TWO, TEST_DESCRIPTION);
        Assert.assertEquals(TEST_NAME_TWO, PROJECT_ENDPOINT.findProjectById(SESSION, TEST_PROJECT_ID).getName());
    }

}
