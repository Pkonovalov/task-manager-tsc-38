package ru.pkonovalov.tm.exception.entity;

import ru.pkonovalov.tm.exception.AbstractException;

public class StatusNotFoundException extends AbstractException {

    public StatusNotFoundException() {
        super("Error! Status not found...");
    }

}
