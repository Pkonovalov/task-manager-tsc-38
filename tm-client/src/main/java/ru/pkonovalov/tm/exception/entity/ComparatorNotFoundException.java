package ru.pkonovalov.tm.exception.entity;

import ru.pkonovalov.tm.exception.AbstractException;

public class ComparatorNotFoundException extends AbstractException {

    public ComparatorNotFoundException() {
        super("Error! Comparator not found...");
    }

}
