package ru.pkonovalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.endpoint.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminUserEndpoint {

    @WebMethod
    void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

}
