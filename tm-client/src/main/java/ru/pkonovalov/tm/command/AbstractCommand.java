package ru.pkonovalov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.service.EndpointLocator;
import ru.pkonovalov.tm.endpoint.Role;

@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected EndpointLocator endpointLocator;

    @Nullable
    public abstract String commandArg();

    @Nullable
    public abstract String commandDescription();

    @NotNull
    public abstract String commandName();

    @Nullable
    public Role[] commandRoles() {
        return null;
    }

    public abstract void execute();

    public void setEndpointLocator(@NotNull final EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }

}