package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.endpoint.Status;
import ru.pkonovalov.tm.exception.system.IndexIncorrectException;
import ru.pkonovalov.tm.util.TerminalUtil;

import java.util.Arrays;

import static ru.pkonovalov.tm.util.ValidationUtil.checkIndex;

public final class TaskByIndexSetStatusCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Set task status by index";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-set-status-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber();
        if (!checkIndex(index, endpointLocator.getTaskEndpoint().countTask(endpointLocator.getSession())))
            throw new IndexIncorrectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        endpointLocator.getTaskEndpoint().setTaskStatusByIndex(endpointLocator.getSession(), index, Status.valueOf(TerminalUtil.nextLine()));

    }

}
