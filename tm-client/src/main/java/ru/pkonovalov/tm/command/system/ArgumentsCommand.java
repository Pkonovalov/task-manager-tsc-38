package ru.pkonovalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-a";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show all arguments";
    }

    @NotNull
    @Override
    public String commandName() {
        return "arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @Nullable final Collection<AbstractCommand> arguments = endpointLocator.getCommandService().getArguments();
        assert arguments != null;
        arguments.forEach(e -> System.out.println(e.commandArg()));
    }

}
