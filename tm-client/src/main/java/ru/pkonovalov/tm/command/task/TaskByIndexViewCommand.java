package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class TaskByIndexViewCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "View task by index";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-view-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        showTask(endpointLocator.getTaskEndpoint().findTaskByIndex(endpointLocator.getSession(), TerminalUtil.nextNumber()));
    }

}
