package ru.pkonovalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-c";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show all commands";
    }

    @NotNull
    @Override
    public String commandName() {
        return "commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @Nullable final Collection<AbstractCommand> commands = endpointLocator.getCommandService().getCommands();
        assert commands != null;
        commands.forEach(e -> System.out.println(e.commandName()));
    }

}
