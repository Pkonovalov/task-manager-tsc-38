package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.exception.empty.EmptyNameException;
import ru.pkonovalov.tm.exception.system.IndexIncorrectException;
import ru.pkonovalov.tm.util.TerminalUtil;

import static ru.pkonovalov.tm.util.ValidationUtil.checkIndex;
import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class ProjectByIndexUpdateCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Update project by index";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-update-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER INDEX:]");
        final int index = TerminalUtil.nextNumber();
        if (!checkIndex(index, endpointLocator.getProjectEndpoint().countProject(endpointLocator.getSession())))
            throw new IndexIncorrectException();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        endpointLocator.getProjectEndpoint().updateProjectByIndex(endpointLocator.getSession(), index, name, TerminalUtil.nextLine());
    }

}
