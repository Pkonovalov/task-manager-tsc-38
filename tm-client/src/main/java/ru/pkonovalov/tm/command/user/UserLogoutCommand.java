package ru.pkonovalov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.endpoint.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Log out of the system";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public Role[] commandRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        endpointLocator.getSessionEndpoint().closeSession(endpointLocator.getSession());
        endpointLocator.setSession(null);
    }

}
