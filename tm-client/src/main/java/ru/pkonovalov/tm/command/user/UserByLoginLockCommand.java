package ru.pkonovalov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.endpoint.Role;

import static ru.pkonovalov.tm.util.TerminalUtil.nextLine;

public final class UserByLoginLockCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Lock user by login";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-lock-by-login";
    }

    @NotNull
    @Override
    public Role[] commandRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        endpointLocator.getAdminUserEndpoint().lockUserByLogin(endpointLocator.getSession(), nextLine());
    }

}
