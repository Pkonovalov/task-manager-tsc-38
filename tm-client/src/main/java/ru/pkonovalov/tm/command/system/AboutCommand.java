package ru.pkonovalov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-a";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show developer info";
    }

    @NotNull
    @Override
    public String commandName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
    }

}
