package ru.pkonovalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pkonovalov.tm.AbstractTest;
import ru.pkonovalov.tm.api.repository.IProjectRepository;
import ru.pkonovalov.tm.enumerated.Sort;
import ru.pkonovalov.tm.enumerated.Status;
import ru.pkonovalov.tm.marker.DBCategory;
import ru.pkonovalov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepositoryTest extends AbstractTest {

    @NotNull
    protected static final String TEST_PROJECT_NAME = "Project test name";

    @NotNull
    protected static final String TEST_PROJECT_NAME_TWO = "Z Project test name";

    @NotNull
    private static final IProjectRepository PROJECT_REPOSITORY = BOOTSTRAP.getProjectRepository();

    @Nullable
    private static Project TEST_PROJECT;

    @NotNull
    private static String TEST_PROJECT_ID;

    @Test
    @Category(DBCategory.class)
    public void addAll() {
        initTwo();
        Assert.assertEquals(2, PROJECT_REPOSITORY.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void clear() {
        PROJECT_REPOSITORY.clear(TEST_USER_ID);
        Assert.assertTrue(PROJECT_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
        PROJECT_REPOSITORY.clear();
        Assert.assertTrue(PROJECT_REPOSITORY.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void create() {
        Assert.assertNotNull(TEST_PROJECT);
        Assert.assertNotNull(TEST_PROJECT.getId());
        Assert.assertNotNull(TEST_PROJECT.getName());
        Assert.assertEquals(TEST_PROJECT_NAME, TEST_PROJECT.getName());
        Assert.assertNotNull(PROJECT_REPOSITORY.findAll(TEST_USER_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsById() {
        Assert.assertTrue(PROJECT_REPOSITORY.existsById(TEST_USER_ID, TEST_PROJECT_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsByName() {
        Assert.assertTrue(PROJECT_REPOSITORY.existsByName(TEST_USER_ID, TEST_PROJECT_NAME));
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        Assert.assertEquals(TEST_PROJECT, PROJECT_REPOSITORY.findOneByIndex(TEST_USER_ID, 1));
        Assert.assertEquals(TEST_PROJECT_ID, PROJECT_REPOSITORY.getIdByIndex(TEST_USER_ID, 1));
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        Assert.assertEquals(TEST_PROJECT, PROJECT_REPOSITORY.findOneByName(TEST_USER_ID, TEST_PROJECT_NAME));
        Assert.assertEquals(TEST_PROJECT_ID, PROJECT_REPOSITORY.getIdByName(TEST_USER_ID, TEST_PROJECT_NAME));
    }

    @After
    public void finishTest() {
        BOOTSTRAP.getUserRepository().clear();
        PROJECT_REPOSITORY.clear();
    }

    private void initTwo() {
        PROJECT_REPOSITORY.clear(TEST_USER_ID);
        @NotNull final Project project1 = new Project(TEST_PROJECT_NAME);
        project1.setUserId(TEST_USER_ID);
        project1.setStatus(Status.IN_PROGRESS);
        @NotNull final Project project2 = new Project(TEST_PROJECT_NAME_TWO);
        project2.setUserId(TEST_USER_ID);
        project2.setStatus(Status.COMPLETE);
        @NotNull final List<Project> list = new ArrayList<>();
        list.add(project1);
        list.add(project2);
        PROJECT_REPOSITORY.addAll(list);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        Assert.assertNotNull(TEST_PROJECT);
        PROJECT_REPOSITORY.remove(TEST_PROJECT);
        Assert.assertNull(PROJECT_REPOSITORY.findById(TEST_PROJECT.getId()));
        Assert.assertTrue(PROJECT_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByIndex() {
        initTwo();
        PROJECT_REPOSITORY.removeOneByIndex(TEST_USER_ID, 1);
        Assert.assertEquals(TEST_PROJECT_NAME_TWO, PROJECT_REPOSITORY.findAll(TEST_USER_ID).get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByName() {
        initTwo();
        PROJECT_REPOSITORY.removeOneByName(TEST_USER_ID, TEST_PROJECT_NAME);
        Assert.assertEquals(TEST_PROJECT_NAME_TWO, PROJECT_REPOSITORY.findAll(TEST_USER_ID).get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void sortName() {
        initTwo();
        @NotNull final List<Project> list = PROJECT_REPOSITORY.findAll(TEST_USER_ID, Sort.NAME.getComparator());
        Assert.assertEquals(TEST_PROJECT_NAME, list.get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void sortStarted() {
        initTwo();
        @NotNull final List<Project> list = PROJECT_REPOSITORY.findAll(TEST_USER_ID, Sort.STARTED.getComparator());
        Assert.assertEquals(TEST_PROJECT_NAME, list.get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void sortStatus() {
        initTwo();
        @NotNull final List<Project> list = PROJECT_REPOSITORY.findAll(TEST_USER_ID, Sort.STATUS.getComparator());
        Assert.assertEquals(TEST_PROJECT_NAME, list.get(0).getName());
    }

    @Before
    @Category(DBCategory.class)
    public void startTest() {
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final Project project = new Project();
        TEST_PROJECT_ID = project.getId();
        project.setName(TEST_PROJECT_NAME);
        project.setUserId(TEST_USER_ID);
        PROJECT_REPOSITORY.add(project);
        TEST_PROJECT = PROJECT_REPOSITORY.findById(TEST_USER_ID, project.getId());
    }

}
