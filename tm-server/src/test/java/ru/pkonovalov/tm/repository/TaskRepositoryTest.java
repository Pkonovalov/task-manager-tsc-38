package ru.pkonovalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pkonovalov.tm.AbstractTest;
import ru.pkonovalov.tm.api.repository.ITaskRepository;
import ru.pkonovalov.tm.enumerated.Sort;
import ru.pkonovalov.tm.enumerated.Status;
import ru.pkonovalov.tm.marker.DBCategory;
import ru.pkonovalov.tm.model.Project;
import ru.pkonovalov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepositoryTest extends AbstractTest {

    @NotNull
    protected static final String TEST_TASK_NAME = "Task test name";

    @NotNull
    protected static final String TEST_TASK_NAME_TWO = "Z Task test name";

    @NotNull
    private static final ITaskRepository TASK_REPOSITORY = BOOTSTRAP.getTaskRepository();

    @Nullable
    private static Task TEST_TASK;

    @NotNull
    private static String TEST_TASK_ID;

    @Test
    @Category(DBCategory.class)
    public void addAll() {
        initTwo();
        Assert.assertEquals(2, TASK_REPOSITORY.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void bindByProjectId() {
        @NotNull final String projectId = createProject();
        TASK_REPOSITORY.bindTaskPyProjectId(TEST_USER_ID, projectId, TEST_TASK_ID);
        Assert.assertNotNull(TEST_TASK);
        Assert.assertEquals(projectId, TEST_TASK.getProjectId());
    }

    @Test
    @Category(DBCategory.class)
    public void clear() {
        TASK_REPOSITORY.clear(TEST_USER_ID);
        Assert.assertTrue(TASK_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
        TASK_REPOSITORY.clear();
        Assert.assertTrue(TASK_REPOSITORY.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void create() {
        Assert.assertNotNull(TEST_TASK);
        Assert.assertNotNull(TEST_TASK.getId());
        Assert.assertNotNull(TEST_TASK.getName());
        Assert.assertEquals(TEST_TASK_NAME, TEST_TASK.getName());
        Assert.assertNotNull(TASK_REPOSITORY.findAll(TEST_USER_ID));
    }

    @NotNull
    private String createProject() {
        @NotNull final Project project = new Project();
        BOOTSTRAP.getProjectRepository().add(project);
        return project.getId();
    }

    @Test
    @Category(DBCategory.class)
    public void existsById() {
        Assert.assertTrue(TASK_REPOSITORY.existsById(TEST_USER_ID, TEST_TASK_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsByName() {
        Assert.assertTrue(TASK_REPOSITORY.existsByName(TEST_USER_ID, TEST_TASK_NAME));
    }

    @Test
    @Category(DBCategory.class)
    public void existsByProjectId() {
        @NotNull final String projectId = createProject();
        TASK_REPOSITORY.bindTaskPyProjectId(TEST_USER_ID, projectId, TEST_TASK_ID);
        Assert.assertTrue(TASK_REPOSITORY.existsByProjectId(TEST_USER_ID, projectId));
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        Assert.assertEquals(TEST_TASK, TASK_REPOSITORY.findOneByIndex(TEST_USER_ID, 1));
        Assert.assertEquals(TEST_TASK_ID, TASK_REPOSITORY.getIdByIndex(TEST_USER_ID, 1));
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        Assert.assertEquals(TEST_TASK, TASK_REPOSITORY.findOneByName(TEST_USER_ID, TEST_TASK_NAME));
    }

    @After
    public void finishTest() {
        BOOTSTRAP.getUserRepository().clear();
        TASK_REPOSITORY.clear();
        BOOTSTRAP.getProjectRepository().clear();
    }

    private void initTwo() {
        TASK_REPOSITORY.clear(TEST_USER_ID);
        @NotNull final Task task1 = new Task(TEST_TASK_NAME);
        task1.setUserId(TEST_USER_ID);
        task1.setStatus(Status.IN_PROGRESS);
        @NotNull final Task task2 = new Task(TEST_TASK_NAME_TWO);
        task2.setUserId(TEST_USER_ID);
        task2.setStatus(Status.COMPLETE);
        @NotNull final List<Task> list = new ArrayList<>();
        list.add(task1);
        list.add(task2);
        TASK_REPOSITORY.addAll(list);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        Assert.assertNotNull(TEST_TASK);
        TASK_REPOSITORY.remove(TEST_TASK);
        Assert.assertNull(TASK_REPOSITORY.findById(TEST_TASK.getId()));
        Assert.assertTrue(TASK_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByIndex() {
        initTwo();
        TASK_REPOSITORY.removeOneByIndex(TEST_USER_ID, 1);
        Assert.assertEquals(TEST_TASK_NAME_TWO, TASK_REPOSITORY.findAll(TEST_USER_ID).get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByName() {
        initTwo();
        TASK_REPOSITORY.removeOneByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertEquals(TEST_TASK_NAME_TWO, TASK_REPOSITORY.findAll(TEST_USER_ID).get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void sortName() {
        initTwo();
        @NotNull final List<Task> list = TASK_REPOSITORY.findAll(TEST_USER_ID, Sort.NAME.getComparator());
        Assert.assertEquals(TEST_TASK_NAME, list.get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void sortStarted() {
        initTwo();
        @NotNull final List<Task> list = TASK_REPOSITORY.findAll(TEST_USER_ID, Sort.STARTED.getComparator());
        Assert.assertEquals(TEST_TASK_NAME, list.get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void sortStatus() {
        initTwo();
        @NotNull final List<Task> list = TASK_REPOSITORY.findAll(TEST_USER_ID, Sort.STATUS.getComparator());
        Assert.assertEquals(TEST_TASK_NAME, list.get(0).getName());
    }

    @Before
    public void startTest() {
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final Task task = new Task();
        TEST_TASK_ID = task.getId();
        task.setName(TEST_TASK_NAME);
        task.setUserId(TEST_USER_ID);
        TASK_REPOSITORY.add(task);
        TEST_TASK = TASK_REPOSITORY.findById(TEST_USER_ID, task.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void unbind() {
        @NotNull final String projectId = createProject();
        TASK_REPOSITORY.bindTaskPyProjectId(TEST_USER_ID, projectId, TEST_TASK_ID);
        TASK_REPOSITORY.unbindTaskFromProject(TEST_USER_ID, TEST_TASK_ID);
        Assert.assertNotNull(TEST_TASK);
        Assert.assertNull(TEST_TASK.getProjectId());
    }

}
