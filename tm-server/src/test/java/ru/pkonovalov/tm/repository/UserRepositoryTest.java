package ru.pkonovalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pkonovalov.tm.AbstractTest;
import ru.pkonovalov.tm.api.repository.IUserRepository;
import ru.pkonovalov.tm.marker.DBCategory;

public class UserRepositoryTest extends AbstractTest {

    @NotNull
    private static final String TEST_PASSWORD = "test-password";
    @NotNull
    private static final IUserRepository USER_REPOSITORY = BOOTSTRAP.getUserRepository();

    @Test
    public void existByEmail() {
        Assert.assertTrue(USER_REPOSITORY.existsByEmail(TEST_USER_EMAIL));
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(TEST_USER, USER_REPOSITORY.findByLogin(TEST_USER_NAME));
    }

    @After
    public void finishTest() {
        BOOTSTRAP.getUserRepository().clear();
    }

    @Test
    @Category(DBCategory.class)
    public void removeByLogin() {
        USER_REPOSITORY.removeByLogin(TEST_USER_NAME);
        Assert.assertTrue(USER_REPOSITORY.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void setPassword() {
        USER_REPOSITORY.setPasswordById(TEST_USER_ID, TEST_PASSWORD);
        Assert.assertEquals(TEST_PASSWORD, USER_REPOSITORY.findByLogin(TEST_USER_NAME).getPasswordHash());
    }

    @Before
    public void startTest() {
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
    }

}
