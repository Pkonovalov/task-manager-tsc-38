package ru.pkonovalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pkonovalov.tm.AbstractTest;
import ru.pkonovalov.tm.api.repository.ISessionRepository;
import ru.pkonovalov.tm.marker.DBCategory;
import ru.pkonovalov.tm.model.Session;

public class SessionRepositoryTest extends AbstractTest {

    @NotNull
    private static final ISessionRepository SESSION_REPOSITORY = BOOTSTRAP.getSessionRepository();

    @Nullable
    private static Session TEST_SESSION;

    @NotNull
    private static String TEST_SESSION_ID;

    @Test
    @Category(DBCategory.class)
    public void clear() {
        SESSION_REPOSITORY.clear(TEST_USER_ID);
        Assert.assertTrue(SESSION_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
        SESSION_REPOSITORY.clear();
        Assert.assertTrue(SESSION_REPOSITORY.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void findByUserId() {
        Assert.assertFalse(SESSION_REPOSITORY.findByUserId(TEST_USER_ID).isEmpty());
        Assert.assertEquals(SESSION_REPOSITORY.findByUserId(TEST_USER_ID).get(0), TEST_SESSION);
    }

    @After
    public void finishTest() {
        BOOTSTRAP.getUserRepository().clear();
        SESSION_REPOSITORY.clear();
    }

    @Test
    @Category(DBCategory.class)
    public void removeByUserId() {
        SESSION_REPOSITORY.removeByUserId(TEST_USER_ID);
        Assert.assertEquals(0, SESSION_REPOSITORY.size(TEST_USER_ID));
    }

    @Before
    @Category(DBCategory.class)
    public void startTest() {
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final Session session = new Session();
        TEST_SESSION_ID = session.getId();
        session.setUserId(TEST_USER_ID);
        SESSION_REPOSITORY.add(session);
        TEST_SESSION = SESSION_REPOSITORY.findById(TEST_SESSION_ID);
    }
}
