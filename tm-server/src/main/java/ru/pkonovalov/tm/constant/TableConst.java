package ru.pkonovalov.tm.constant;

import org.jetbrains.annotations.NotNull;

public class TableConst {

    @NotNull
    public static final String PROJECT_TABLE = "app_project";

    @NotNull
    public static final String SESSION_TABLE = "app_session";

    @NotNull
    public static final String TASK_TABLE = "app_task";

    @NotNull
    public static final String USER_TABLE = "app_user";

}
