package ru.pkonovalov.tm.constant;

import org.jetbrains.annotations.NotNull;

public class FieldConst {

    @NotNull
    public static final String DATE_FINISH = "dateFinish";
    @NotNull
    public static final String DATE_START = "dateStart";
    @NotNull
    public static final String DESCRIPTION = "description";
    @NotNull
    public static final String EMAIL = "email";
    @NotNull
    public static final String FIRST_NAME = "firstName";
    @NotNull
    public static final String ID = "id";
    @NotNull
    public static final String LAST_NAME = "lastName";
    @NotNull
    public static final String LOCKED = "locked";
    @NotNull
    public static final String LOGIN = "login";
    @NotNull
    public static final String MID_NAME = "midName";
    @NotNull
    public static final String NAME = "name";
    @NotNull
    public static final String PASSWORD_HASH = "passwordHash";
    @NotNull
    public static final String PROJECT_ID = "projectId";
    @NotNull
    public static final String ROLE = "role";
    @NotNull
    public static final String SIGNATURE = "signature";
    @NotNull
    public static final String TIMESTAMP = "timestamp";
    @NotNull
    public static final String USER_ID = "userId";

}
