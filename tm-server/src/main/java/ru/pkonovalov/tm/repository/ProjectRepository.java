package ru.pkonovalov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.IProjectRepository;
import ru.pkonovalov.tm.constant.FieldConst;
import ru.pkonovalov.tm.constant.TableConst;
import ru.pkonovalov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
        super.table = TableConst.PROJECT_TABLE;
    }

    @Nullable
    @SneakyThrows
    public Project add(@Nullable final Project project) {
        if (project == null) return null;
        if (isEmpty(project.getId())) return null;
        @NotNull final String query =
                "INSERT INTO `" + getTable() + "` " +
                        "(`" + FieldConst.NAME + "`, `" + FieldConst.DESCRIPTION + "`, `" + FieldConst.DATE_START + "`, `" + FieldConst.DATE_FINISH + "`, `" + FieldConst.USER_ID + "`, `" + FieldConst.ID + "`) " +
                        "VALUES (?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, project.getName());
        statement.setString(2, project.getDescription());
        statement.setDate(3, prepare(project.getDateStart()));
        statement.setDate(4, prepare(project.getDateFinish()));
        statement.setString(5, project.getUserId());
        statement.setString(6, project.getId());
        statement.execute();
        return project;
    }

    public void createProject(@Nullable String name) {
        @NotNull Project project = new Project();
        project.setName(name);
        add(project);
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (isEmpty(id)) return false;
        @NotNull final String query = "SELECT COUNT(*) FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @Override
    @SneakyThrows
    public boolean existsByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmpty(name)) return false;
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.NAME + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final boolean hasNext = resultSet.next();
        statement.close();
        if (!hasNext) return false;
        return true;
    }

    @Nullable
    @SneakyThrows
    protected Project fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString(FieldConst.ID));
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setDateStart(row.getDate(FieldConst.DATE_START));
        project.setDateFinish(row.getDate(FieldConst.DATE_FINISH));
        project.setUserId(row.getString(FieldConst.USER_ID));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final Statement statement = getConnection().createStatement();
        @NotNull final String query = "SELECT * FROM `" + getTable() + "`";
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findById(@NotNull String id) {
        if (isEmpty(id)) return null;
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.ID + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index - 1);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        return fetch(resultSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.NAME + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        return fetch(resultSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getIdByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index - 1);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        return resultSet.getString(FieldConst.ID);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getIdByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.NAME + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        return resultSet.getString(FieldConst.ID);
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull String userId, @NotNull String id) {
        if (isEmpty(id)) return;
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, getIdByIndex(userId, index));
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmpty(name)) return;
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.NAME + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        statement.execute();
    }

    @SneakyThrows
    public void update(@Nullable final Project project) {
        if (project == null) return;
        @NotNull final String query =
                "UPDATE `" + getTable() + "` " +
                        "SET `" + FieldConst.NAME + "` = ?, `" + FieldConst.DESCRIPTION + "` = ?, `" + FieldConst.DATE_START + "` = ?, `" + FieldConst.DATE_FINISH + "` = ?, `" + FieldConst.USER_ID + "` = ? " +
                        "WHERE `" + FieldConst.ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, project.getName());
        statement.setString(2, project.getDescription());
        statement.setDate(3, prepare(project.getDateStart()));
        statement.setDate(4, prepare(project.getDateFinish()));
        statement.setString(5, project.getUserId());
        statement.setString(6, project.getId());
        statement.execute();
    }

}
