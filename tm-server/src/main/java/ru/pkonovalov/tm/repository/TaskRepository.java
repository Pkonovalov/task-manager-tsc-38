package ru.pkonovalov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.ITaskRepository;
import ru.pkonovalov.tm.constant.FieldConst;
import ru.pkonovalov.tm.constant.TableConst;
import ru.pkonovalov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
        super.table = TableConst.TASK_TABLE;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task task) {
        if (task == null) return null;
        if (isEmpty(task.getId())) return null;
        @NotNull final String query =
                "INSERT INTO `" + getTable() + "` " +
                        "(`" + FieldConst.NAME + "`, `" + FieldConst.DESCRIPTION + "`, `" + FieldConst.DATE_START + "`, `" + FieldConst.DATE_FINISH + "`, `" + FieldConst.USER_ID + "`, `" + FieldConst.ID + "`, `" + FieldConst.PROJECT_ID + "`) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, task.getName());
        statement.setString(2, task.getDescription());
        statement.setDate(3, prepare(task.getDateStart()));
        statement.setDate(4, prepare(task.getDateFinish()));
        statement.setString(5, task.getUserId());
        statement.setString(6, task.getId());
        statement.setString(7, task.getProjectId());
        statement.execute();
        return task;
    }

    @Override
    @SneakyThrows
    public void bindTaskPyProjectId(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        @NotNull final String query =
                "UPDATE `" + getTable() + "` " +
                        "SET `" + FieldConst.PROJECT_ID + "` = ? " +
                        "WHERE `" + FieldConst.ID + "` = ? AND `" + FieldConst.USER_ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, taskId);
        statement.setString(3, userId);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public boolean existsByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT COUNT(*) FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.NAME + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @Override
    @SneakyThrows
    public boolean existsByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "SELECT COUNT(*) FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.PROJECT_ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @Nullable
    @SneakyThrows
    protected Task fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(FieldConst.ID));
        task.setName(row.getString(FieldConst.NAME));
        task.setDescription(row.getString(FieldConst.DESCRIPTION));
        task.setDateStart(row.getDate(FieldConst.DATE_START));
        task.setDateFinish(row.getDate(FieldConst.DATE_FINISH));
        task.setProjectId(row.getString(FieldConst.PROJECT_ID));
        task.setUserId(row.getString(FieldConst.USER_ID));
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.PROJECT_ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index - 1);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        return fetch(resultSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.NAME + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        return fetch(resultSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getIdByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index - 1);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        return resultSet.getString(FieldConst.ID);
    }

    @Override
    @SneakyThrows
    public void removeAllBinded(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.PROJECT_ID + "` IS NOT NULL";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate(query);
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.PROJECT_ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.executeUpdate(query);
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, getIdByIndex(userId, index));
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmpty(name)) return;
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.NAME + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        statement.execute();
    }

    @Override
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String taskId) {
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) return;
        task.setProjectId(null);
        update(task);
    }

    @SneakyThrows
    public void update(@Nullable final Task task) {
        if (task == null) return;
        @NotNull final String query =
                "UPDATE `" + getTable() + "` " +
                        "SET `" + FieldConst.NAME + "` = ?, `" + FieldConst.DESCRIPTION + "` = ?, `" + FieldConst.DATE_START + "` = ?, `" + FieldConst.DATE_FINISH + "` = ?, `" + FieldConst.USER_ID + "` = ?, `" + FieldConst.PROJECT_ID + "` = ? " +
                        "WHERE `" + FieldConst.ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, task.getName());
        statement.setString(2, task.getDescription());
        statement.setDate(3, prepare(task.getDateStart()));
        statement.setDate(4, prepare(task.getDateFinish()));
        statement.setString(5, task.getUserId());
        statement.setString(6, task.getProjectId());
        statement.setString(7, task.getId());
        statement.execute();
    }

}
