package ru.pkonovalov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.ISessionRepository;
import ru.pkonovalov.tm.constant.FieldConst;
import ru.pkonovalov.tm.constant.TableConst;
import ru.pkonovalov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
        super.table = TableConst.SESSION_TABLE;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session add(@Nullable Session session) {
        if (session == null) return null;
        if (isEmpty(session.getId())) return null;
        @NotNull final String query =
                "INSERT INTO `" + getTable() + "` " +
                        "(`" + FieldConst.ID + "`, `" + FieldConst.TIMESTAMP + "`, `" + FieldConst.SIGNATURE + "`, `" + FieldConst.USER_ID + "`) " +
                        "VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setLong(2, session.getTimestamp());
        statement.setString(3, session.getSignature());
        statement.setString(4, session.getUserId());
        statement.execute();
        return session;
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        @NotNull final String query =
                "SELECT * FROM " + getTable() + " " +
                        "WHERE `" + FieldConst.ID + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        final boolean result = statement.executeQuery().first();
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    protected Session fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString(FieldConst.ID));
        session.setTimestamp(row.getLong(FieldConst.TIMESTAMP));
        session.setSignature(row.getString(FieldConst.SIGNATURE));
        session.setUserId(row.getString(FieldConst.USER_ID));
        return session;
    }

    @Override
    @SneakyThrows
    public List<Session> findByUserId(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Session> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeByUserId(@NotNull String userId) {
        if (isEmpty(userId)) return;
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void update(@Nullable Session session) {
        if (session == null) return;
        @NotNull final String query =
                "UPDATE `" + getTable() + "` " +
                        "SET `" + FieldConst.TIMESTAMP + "` = ?, `" + FieldConst.SIGNATURE + "` = ?, `" + FieldConst.USER_ID + "` = ? " +
                        "WHERE `" + FieldConst.ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setLong(1, session.getTimestamp());
        statement.setString(2, session.getSignature());
        statement.setString(3, session.getUserId());
        statement.setString(4, session.getId());
        statement.execute();
    }

}
