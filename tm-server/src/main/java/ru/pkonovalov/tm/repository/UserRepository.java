package ru.pkonovalov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.IUserRepository;
import ru.pkonovalov.tm.constant.FieldConst;
import ru.pkonovalov.tm.constant.TableConst;
import ru.pkonovalov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
        super.table = TableConst.USER_TABLE;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User add(@Nullable final User entity) {
        if (entity == null) return null;
        @NotNull final String query = "INSERT INTO " + getTable() +
                "(`" + FieldConst.ID + "`, `" + FieldConst.EMAIL + "`, `" + FieldConst.LOGIN + "`, `" + FieldConst.ROLE + "`, `" + FieldConst.LOCKED + "`, " +
                "`" + FieldConst.FIRST_NAME + "`, `" + FieldConst.LAST_NAME + "`, `" + FieldConst.MID_NAME + "`, `" + FieldConst.PASSWORD_HASH + "`)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getEmail());
        statement.setString(3, entity.getLogin());
        statement.setString(4, entity.getRole().toString());
        statement.setBoolean(5, entity.isLocked());
        statement.setString(6, entity.getFirstName());
        statement.setString(7, entity.getLastName());
        statement.setString(8, entity.getMiddleName());
        statement.setString(9, entity.getPasswordHash());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    @SneakyThrows
    public boolean existsByEmail(@Nullable final String email) {
        @NotNull final String query =
                "SELECT * FROM " + getTable() + " " +
                        "WHERE `" + FieldConst.EMAIL + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, email);
        final boolean result = statement.executeQuery().first();
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull String id) {
        @NotNull final String query =
                "SELECT * FROM " + getTable() + " " +
                        "WHERE `" + FieldConst.ID + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        final boolean result = statement.executeQuery().first();
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsByLogin(@Nullable final String login) {
        @NotNull final String query =
                "SELECT * FROM " + getTable() + " " +
                        "WHERE `" + FieldConst.LOGIN + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, login);
        final boolean result = statement.executeQuery().first();
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    protected User fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString(FieldConst.ID));
        user.setLogin(row.getString(FieldConst.LOGIN));
        user.setPasswordHash(row.getString(FieldConst.PASSWORD_HASH));
        user.setRole(prepare(row.getString(FieldConst.ROLE)));
        user.setEmail(row.getString(FieldConst.EMAIL));
        user.setLocked(row.getBoolean(FieldConst.LOCKED));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        if (isEmpty(login)) return null;
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.LOGIN + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@NotNull final String login) {
        if (isEmpty(login)) return;
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.LOGIN + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, login);
        statement.execute();
    }

    @Override
    public void setPasswordById(@NotNull final String id, @NotNull final String passwordHash) {
        @Nullable final User user = findById(id);
        if (user == null) return;
        user.setPasswordHash(passwordHash);
        update(user);
    }

    @SneakyThrows
    public void update(@Nullable final User user) {
        if (user == null) return;
        @NotNull final String query =
                "UPDATE `app_user` " +
                        "SET `" + FieldConst.LOGIN + "` = ?, `" + FieldConst.PASSWORD_HASH + "` = ?, `" + FieldConst.USER_ID + "` = ?, `" + FieldConst.EMAIL + "` = ? " +
                        "WHERE `" + FieldConst.ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPasswordHash());
        statement.setString(3, user.getId());
        statement.setString(4, user.getEmail());
        statement.execute();
    }

}
