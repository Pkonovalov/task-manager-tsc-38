package ru.pkonovalov.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IRepository;
import ru.pkonovalov.tm.constant.FieldConst;
import ru.pkonovalov.tm.enumerated.Role;
import ru.pkonovalov.tm.model.AbstractEntity;
import ru.pkonovalov.tm.model.Project;

import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.checkRole;

@Getter
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @Nullable
    protected String table;
    @NotNull
    private Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public void addAll(@Nullable List<E> entityList) {
        if (entityList == null) return;
        for (@NotNull final E e : entityList)
            add(e);
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate(query);
        statement.close();
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String query = "DELETE FROM `" + getTable() + "`";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.executeUpdate(query);
        statement.close();
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query =
                "SELECT * FROM " + getTable() + " " +
                        "WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.ID + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        final boolean result = statement.executeQuery().first();
        statement.close();
        return result;
    }

    @Nullable
    protected abstract E fetch(@Nullable final ResultSet resultSet);

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final String query = "SELECT * FROM " + getTable();
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@NotNull final String id) {
        @NotNull final String query = "SELECT * FROM " + getTable() + " WHERE id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @Nullable final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "SELECT * FROM " + getTable() + " WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.ID + "` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @Nullable final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    public Date prepare(@Nullable final java.util.Date date) {
        if (date == null) return null;
        return new Date(date.getTime());
    }

    @Nullable
    public Role prepare(@Nullable final String s) {
        if (!checkRole(s)) return null;
        return Role.valueOf(s);
    }

    @Nullable
    public String prepare(@Nullable final Role role) {
        if (role == null) return null;
        return role.toString();
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final E e) {
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, e.getId());
        statement.executeUpdate(query);
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "DELETE FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ? AND `" + FieldConst.ID + "` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.executeUpdate(query);
        statement.close();
    }

    @Override
    @SneakyThrows
    public int size(@NotNull String userId) {
        int count = 0;
        @NotNull final String query = "SELECT * FROM `" + getTable() + "` WHERE `" + FieldConst.USER_ID + "` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) count++;
        statement.close();
        return count;
    }

    @Override
    @SneakyThrows
    public int size() {
        int count = 0;
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final String query = "SELECT * FROM " + getTable() + "`";
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) count++;
        statement.close();
        return count;
    }

}
