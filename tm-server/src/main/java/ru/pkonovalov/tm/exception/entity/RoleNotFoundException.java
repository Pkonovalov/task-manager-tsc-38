package ru.pkonovalov.tm.exception.entity;

import ru.pkonovalov.tm.exception.AbstractException;

public class RoleNotFoundException extends AbstractException {

    public RoleNotFoundException() {
        super("Error! Role not found...");
    }

}
