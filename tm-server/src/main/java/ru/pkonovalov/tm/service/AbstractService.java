package ru.pkonovalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IRepository;
import ru.pkonovalov.tm.api.IService;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.exception.empty.EmptyIdException;
import ru.pkonovalov.tm.exception.entity.EntityNotFoundException;
import ru.pkonovalov.tm.exception.entity.ProjectNotFoundException;
import ru.pkonovalov.tm.model.AbstractEntity;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public E add(@Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.add(entity);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<E> list) {
        if (list == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.addAll(list);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.existsById(userId, id);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll(userId, comparator);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll(userId);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll();
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findById(userId, id);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@NotNull final String id) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findById(id);
        } finally {
            connection.close();
        }
    }

    @NotNull
    protected abstract IRepository<E> getRepository(@NotNull final Connection connection);

    @Override
    @SneakyThrows
    public void remove(@Nullable final E entity) {
        if (entity == null) throw new ProjectNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.remove(entity);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.removeById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public int size() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.size();
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public int size(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            if (repository.size() == 0) return 0;
            return repository.size(userId);
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public void update(@Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.update(entity);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
