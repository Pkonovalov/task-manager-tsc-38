package ru.pkonovalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.api.service.IProjectTaskService;
import ru.pkonovalov.tm.exception.empty.EmptyIdException;
import ru.pkonovalov.tm.exception.empty.EmptyNameException;
import ru.pkonovalov.tm.exception.entity.ProjectNotFoundException;
import ru.pkonovalov.tm.exception.system.IndexIncorrectException;
import ru.pkonovalov.tm.model.Task;
import ru.pkonovalov.tm.repository.ProjectRepository;
import ru.pkonovalov.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.checkIndex;
import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (isEmpty(projectId) || isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            taskRepository.bindTaskPyProjectId(userId, projectId, taskId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clearTasks(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.removeAllBinded(userId);
            projectRepository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            if (!taskRepository.existsByProjectId(userId, projectId)) return null;
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @NotNull final Integer projectIndex) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            if (!checkIndex(projectIndex, projectRepository.size(userId))) throw new IndexIncorrectException();
            @Nullable final String projectId = projectRepository.getIdByIndex(userId, projectIndex);
            if (isEmpty(projectId)) throw new EmptyIdException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeOneByIndex(userId, projectIndex);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String projectName) {
        if (isEmpty(projectName)) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final String projectId = projectRepository.getIdByName(userId, projectName);
            if (isEmpty(projectId)) throw new EmptyIdException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeOneByName(userId, projectName);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@NotNull final String userId, @Nullable final String taskId) {
        if (isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.unbindTaskFromProject(userId, taskId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
