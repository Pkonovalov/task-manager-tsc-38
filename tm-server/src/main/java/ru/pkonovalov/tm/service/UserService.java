package ru.pkonovalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IRepository;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.api.service.IPropertyService;
import ru.pkonovalov.tm.api.service.IUserService;
import ru.pkonovalov.tm.enumerated.Role;
import ru.pkonovalov.tm.exception.empty.EmptyEmailException;
import ru.pkonovalov.tm.exception.empty.EmptyIdException;
import ru.pkonovalov.tm.exception.empty.EmptyLoginException;
import ru.pkonovalov.tm.exception.empty.EmptyPasswordException;
import ru.pkonovalov.tm.exception.entity.UserNotFoundException;
import ru.pkonovalov.tm.exception.user.EmailExistsException;
import ru.pkonovalov.tm.exception.user.LoginExistsException;
import ru.pkonovalov.tm.model.User;
import ru.pkonovalov.tm.repository.UserRepository;
import ru.pkonovalov.tm.util.HashUtil;

import java.sql.Connection;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IConnectionService connectionService;
    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            if (userRepository.existsByLogin(login)) throw new LoginExistsException();
            if (userRepository.existsByEmail(email)) throw new EmailExistsException();
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setEmail(email);
            add(user);
            return user;
        } finally {
            connection.commit();
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @NotNull final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            if (userRepository.existsByLogin(login)) throw new LoginExistsException();
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(role);
            userRepository.add(user);
            return user;
        } finally {
            connection.commit();
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsByEmail(@Nullable final String email) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            return userRepository.existsByEmail(email);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsByLogin(@Nullable final String login) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            return userRepository.existsByLogin(login);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            return userRepository.findByLogin(login);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    protected IRepository<User> getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            update(user);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return;
        remove(user);
    }

    @Override
    @SneakyThrows
    public void setPassword(@NotNull final String userId, @Nullable final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            update(user);
        } finally {
            connection.close();
        }

    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) throw new UserNotFoundException();
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setLastName(lastName);
            update(user);
        } finally {
            connection.close();
        }
    }

}
