package ru.pkonovalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IRepository;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.api.service.IPropertyService;
import ru.pkonovalov.tm.api.service.ISessionService;
import ru.pkonovalov.tm.enumerated.Role;
import ru.pkonovalov.tm.exception.user.AccessDeniedException;
import ru.pkonovalov.tm.model.Session;
import ru.pkonovalov.tm.model.User;
import ru.pkonovalov.tm.repository.SessionRepository;
import ru.pkonovalov.tm.repository.UserRepository;
import ru.pkonovalov.tm.util.HashUtil;
import ru.pkonovalov.tm.util.SignatureUtil;

import java.sql.Connection;
import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;
    @NotNull
    private final IPropertyService propertyService;

    public SessionService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        super(connectionService);
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login) || isEmpty(password)) return false;
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) return false;
            if (user.isLocked()) return false;
            @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
            if (isEmpty(passwordHash)) return false;
            return passwordHash.equals(user.getPasswordHash());
        }
    }

    @Override
    public void close(@NotNull final Session session) {
        validate(session);
        remove(session);
    }

    @Override
    @SneakyThrows
    public void closeAll(@NotNull final Session session) {
        validate(session);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final SessionRepository sessionRepository = new SessionRepository(connection);
            sessionRepository.removeByUserId(session.getUserId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Session> getListSession(@NotNull final Session session) {
        validate(session);
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final SessionRepository sessionRepository = new SessionRepository(connection);
            return sessionRepository.findByUserId(session.getUserId());
        }
    }

    @Override
    protected @NotNull IRepository<Session> getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User getUser(@NotNull final Session session) {
        @NotNull final String userId = getUserId(session);
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            return userRepository.findById(userId);
        }
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final Session session) {
        validate(session);
        return session.getUserId();
    }

    @Override
    public boolean isValid(@NotNull final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check || isEmpty(login)) return null;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) return null;
            @NotNull final Session session = new Session();
            session.setUserId(user.getId());
            session.setTimestamp(System.currentTimeMillis());
            add(session);
            connection.commit();
            return sign(session);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String salt = propertyService.getSessionSalt();
        final int cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        update(session);
        return session;
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) throw new AccessDeniedException();
            if (user.getRole() == null) throw new AccessDeniedException();
            if (!role.equals(user.getRole())) throw new AccessDeniedException();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionSign = sign(temp);
        if (sessionSign == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final SessionRepository sessionRepository = new SessionRepository(connection);
            if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
        }
    }
}
