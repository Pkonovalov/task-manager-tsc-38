package ru.pkonovalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IRepository;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.api.service.IProjectService;
import ru.pkonovalov.tm.enumerated.Status;
import ru.pkonovalov.tm.exception.empty.EmptyIdException;
import ru.pkonovalov.tm.exception.empty.EmptyNameException;
import ru.pkonovalov.tm.exception.entity.ProjectNotFoundException;
import ru.pkonovalov.tm.exception.system.IndexIncorrectException;
import ru.pkonovalov.tm.model.Project;
import ru.pkonovalov.tm.repository.ProjectRepository;

import java.sql.Connection;

import static ru.pkonovalov.tm.util.ValidationUtil.checkIndex;
import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    @SneakyThrows
    public boolean existsByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.existsByName(userId, name);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            if (!checkIndex(index, projectRepository.size())) throw new IndexIncorrectException();
            return projectRepository.findOneByIndex(userId, index);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findOneByName(userId, name);
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishProjectById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        update(project);
    }

    @Override
    public void finishProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        update(project);
    }

    @Override
    public void finishProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        update(project);
    }

    @NotNull
    @Override
    protected IRepository<Project> getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @Override
    public void setProjectStatusById(@NotNull final String userId, @Nullable final String id, @NotNull final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
    }

    @Override
    public void setProjectStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
    }

    @Override
    public void setProjectStatusByName(@NotNull final String userId, @Nullable final String name, @NotNull final Status status) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
    }

    @Override
    public void startProjectById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        update(project);
    }

    @Override
    public void startProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        update(project);
    }

    @Override
    public void startProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        update(project);
    }

    @Override
    public void updateProjectById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        update(project);
    }

    @Override
    public void updateProjectByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
    }

    @Override
    public void updateProjectByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String nameNew,
            @Nullable final String description
    ) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(nameNew);
        project.setDescription(description);
        update(project);
    }

}
