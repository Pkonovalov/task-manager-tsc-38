package ru.pkonovalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IRepository;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.api.service.ITaskService;
import ru.pkonovalov.tm.enumerated.Status;
import ru.pkonovalov.tm.exception.empty.EmptyIdException;
import ru.pkonovalov.tm.exception.empty.EmptyNameException;
import ru.pkonovalov.tm.exception.entity.TaskNotFoundException;
import ru.pkonovalov.tm.exception.system.IndexIncorrectException;
import ru.pkonovalov.tm.model.Task;
import ru.pkonovalov.tm.repository.TaskRepository;

import java.sql.Connection;

import static ru.pkonovalov.tm.util.ValidationUtil.checkIndex;
import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public Task add(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @Override
    @SneakyThrows
    public boolean existsByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.existsByName(userId, name);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            if (!checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
            return taskRepository.findOneByIndex(userId, index);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findOneByName(userId, name);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void finishTaskById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        update(task);
    }

    @Override
    @SneakyThrows
    public void finishTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
            @Nullable final Task task = findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            task.setStatus(Status.COMPLETE);
            update(task);
    }

    @Override
    @SneakyThrows
    public void finishTaskByName(@NotNull final String userId, @Nullable final String name) {
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        update(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getIdByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            if (!checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
            return taskRepository.getIdByIndex(userId, index);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    protected IRepository<Task> getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @Override
    @SneakyThrows
    public void removeOneById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) return;
        remove(findById(userId, id));
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        remove(findOneByIndex(userId, index));
    }

    @Override
    @SneakyThrows
    public void removeOneByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        remove(findOneByName(userId, name));
    }

    @Override
    public void setTaskStatusById(@NotNull final String userId, @Nullable final String id, @NotNull final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
    }

    @Override
    public void setTaskStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
    }

    @Override
    public void setTaskStatusByName(@NotNull final String userId, @Nullable final String name, @NotNull final Status status) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
    }

    @Override
    public void startTaskById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        update(task);
    }

    @Override
    public void startTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        update(task);
    }

    @Override
    public void startTaskByName(@NotNull final String userId, @Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        update(task);
    }

    @Override
    public void updateTaskById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        update(task);
    }

    @Override
    public void updateTaskByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (!checkIndex(index, size(userId))) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
    }

    @Override
    public void updateTaskByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String nameNew,
            @Nullable final String description
    ) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setName(nameNew);
        task.setDescription(description);
        update(task);
    }

}
