package ru.pkonovalov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.endpoint.*;
import ru.pkonovalov.tm.api.repository.*;
import ru.pkonovalov.tm.api.service.*;
import ru.pkonovalov.tm.component.Backup;
import ru.pkonovalov.tm.endpoint.*;
import ru.pkonovalov.tm.enumerated.Role;
import ru.pkonovalov.tm.enumerated.Status;
import ru.pkonovalov.tm.repository.ProjectRepository;
import ru.pkonovalov.tm.repository.SessionRepository;
import ru.pkonovalov.tm.repository.TaskRepository;
import ru.pkonovalov.tm.repository.UserRepository;
import ru.pkonovalov.tm.service.*;
import ru.pkonovalov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;

@Getter
public final class Bootstrap implements ServiceLocator, ConnectionProvider {

    @NotNull
    private final static String PID_FILENAME = "task-manager.pid";
    @NotNull
    private final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);
    @NotNull
    private final ConnectionProvider connectionProvider = this;
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);
    @NotNull
    private final Backup backup = new Backup(this, propertyService);
    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this, backup);
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, propertyService);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);
    @Nullable
    private Connection connection = connectionService.getConnection();
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connection);
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository(connection);
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(connection);
    @NotNull
    private final IUserRepository userRepository = new UserRepository(connection);

    private void initData() {
        @NotNull final String idAdmin = userService.create("admin", "admin", Role.ADMIN).getId();
        @NotNull final String idTest = userService.create("test", "test", "test@test.ru").getId();

        projectService.add(idTest, "proj1", "desc1").setStatus(Status.COMPLETE);
        projectService.add(idTest, "proj2", "desc2").setStatus(Status.IN_PROGRESS);
        projectService.add(idTest, "proj3", "desc3").setStatus(Status.IN_PROGRESS);
        projectService.add(idAdmin, "proj4", "desc4").setStatus(Status.NOT_STARTED);
        projectService.add(idAdmin, "proj5", "desc5").setStatus(Status.COMPLETE);
        projectService.add(idAdmin, "proj6", "desc6").setStatus(Status.NOT_STARTED);

        taskService.add(idTest, "task1", "task_desc1").setStatus(Status.COMPLETE);
        taskService.add(idTest, "task2", "task_desc2").setStatus(Status.NOT_STARTED);
        taskService.add(idTest, "task3", "task_desc3").setStatus(Status.IN_PROGRESS);
        taskService.add(idAdmin, "task4", "task_desc4").setStatus(Status.NOT_STARTED);
        taskService.add(idAdmin, "task5", "task_desc5").setStatus(Status.IN_PROGRESS);
        taskService.add(idAdmin, "task6", "task_desc6").setStatus(Status.NOT_STARTED);
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(adminEndpoint);
        registry(adminUserEndpoint);
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(PID_FILENAME), pid.getBytes());
        @NotNull final File file = new File(PID_FILENAME);
        file.deleteOnExit();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        final int port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + "/" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    public void run(@Nullable final String... args) {
        initPID();
        //initData();
        initEndpoint();
        backup.init();
    }

}
