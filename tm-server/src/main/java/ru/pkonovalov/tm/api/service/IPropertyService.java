package ru.pkonovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.api.other.ISaltSettings;

public interface IPropertyService extends ISaltSettings {

    @NotNull String getApplicationVersion();

    int getBackupInterval();

    @NotNull String getJdbcDriver();

    @NotNull String getJdbcPassword();

    @NotNull String getJdbcUrl();

    @NotNull String getJdbcUsername();

    int getScannerInterval();

    @NotNull String getServerHost();

    int getServerPort();

    int getSessionCycle();

    @NotNull String getSessionSalt();

}
