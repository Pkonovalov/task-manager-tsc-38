package ru.pkonovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull IAuthService getAuthService();

    @NotNull IConnectionService getConnectionService();

    @NotNull IProjectService getProjectService();

    @NotNull IProjectTaskService getProjectTaskService();

    @NotNull IPropertyService getPropertyService();

    @NotNull ISessionService getSessionService();

    @NotNull ITaskService getTaskService();

    @NotNull IUserService getUserService();

}
