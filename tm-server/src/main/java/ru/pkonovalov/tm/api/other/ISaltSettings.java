package ru.pkonovalov.tm.api.other;

import org.jetbrains.annotations.NotNull;

public interface ISaltSettings {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();


}
