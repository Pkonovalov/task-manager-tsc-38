package ru.pkonovalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IRepository;
import ru.pkonovalov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void bindTaskPyProjectId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    boolean existsByName(@NotNull String userId, @NotNull String name);

    boolean existsByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAllBinded(@NotNull String userId);

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByName(@NotNull String userId, @NotNull String name);

    void unbindTaskFromProject(@NotNull String userId, @NotNull String taskId);

}
